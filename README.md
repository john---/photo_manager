# Objective

Manage digital photographs and video files via a Web front end.

Description
-----------

# Method

Manage locally stored photographs and videos using file system layout.  Tags (e.g. who is in the picture) are stored as part of the file name.  This allows for tags to be managed outside the application and to retain context without requiring an EXIF viewer.  For example, if files are sent in an email.

Technical
---------

# Metadata cache

A [PostgreSQL(https://www.postgresql.org/) database is used by the front-end to navigate the photos and videos.  It is re-built nightly to ensure it represents current state of the repository.  This allows for the photos managed outside the application to be accurately represented in the database.

# Development tools

Server side application is using the Perl based [Mojolicious](https://mojolicious.org/) framework.  Client side uses [Vue.js](https://vuejs.org/) and [Vuetify](https://vuetifyjs.com/en/).

Back-end tools/run-time environment are installed and running in a Docker container.  The application source code resides on the host computer.

See server/BOOTSTRAP.md for details on how to set up the development environment.
