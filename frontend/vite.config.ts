import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import Path from '@mojojs/path'

const isDev = process.env.MOJO_MODE === 'development'
process.env.NODE_ENV = 'development'


// https://vitejs.dev/config/
// https://docs.mojolicious.org/Mojolicious/Guides/Rendering#toc
// https://github.com/mojolicious/mojo/discussions/2054
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    },
  },
  build: {
   emptyOutDir: true,  // needed when writing directly to mojo public dir
   rollupOptions: {
    output: {
      entryFileNames: isDev ? '[name].development.js' : '[name].[hash].js',
      dir: '../server/public',
      format: 'iife'
    }
   }
 }
})
