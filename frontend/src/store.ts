import { reactive } from 'vue'

export const store = reactive({
  //export_file: ''
  export_file: <string>'',
  export_contents: <{folder: string, photo: string}[]>[]  // folder and photo list
})
