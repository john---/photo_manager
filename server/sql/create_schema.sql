create table folders (
    id serial primary key,
    folder text not null unique
);

grant select, insert on folders to script;
grant usage on sequence folders_id_seq to script;
grant truncate on folders to script;

create table photos (
    id serial primary key, 
    folder_id integer references folders,
    photo text not null,
    image boolean not null,
    created timestamp with time zone,
    unique (folder_id, photo)
);

grant select, insert, update, delete on photos to script;
grant usage on sequence photos_id_seq to script;
grant truncate on photos to script;

create table tags (
    id serial primary key,
    photo_id integer references photos,
    tag text not null,
    unique (photo_id, tag)
); 

grant select, insert, delete on tags to script;
grant usage on sequence tags_id_seq to script;
grant truncate on tags to script;

create or replace view tagged_photos as
select photo, photos.id as photo_id, array_agg (tag order by tags.id asc) tags, created, image, folder, folders.id as folder_id
  from photos
  left outer join tags on photos.id = tags.photo_id
  right outer join folders on folders.id = photos.folder_id
  group by photos.id, folders.id;

grant select on tagged_photos to script;
