# Server container (mojolicious application)

## create the server container

See `~/perl_docker_build_deploy` for server container creation.

This document also has steps for rebuilding container when cpanfile changes.

## Working with the server container

Starting the the server container and viewing logs:
```
cd server
docker compose up -d 
docker compose logs -f
docker logs -f  photomanager
```

# Working with the database

## Datase creation

As user that can create databases:
```
psql
create database photos;
```

## Schema creation
As user that can create and delete tables:

`psql -d photos -f ./sql/create_schema.sql`

## Schema removal
`psql -d photos -f ./sql/remove_schema.sql`

## Reload database from file structure
```
curl -s -S "http://<host>:3103/loaddb"
```
This is run in a cron job to refresh the datbase monthly.

Alternately, restart the server container.

## Database queries
```
insert into folders (name) values ('that time');
insert into folders (name) values ('that time');   # won't work

insert into photos (folder_id, file) values (1, 'blah.jpg');
insert into photos (folder_id, file) values (1, 'blah.jpg');  # won't work due to uniqueness constaint
insert into photos (folder_id, file) values (1, 'the_thing.jpg');

insert into tags (photo_id, tag) values (1, 'rob');
insert into tags (photo_id, tag) values (1, 'rob');  # won't work due to uniqueness constraint
insert into tags (photo_id, tag) values (1, 'mark');
insert into tags (photo_id, tag) values (3, 'rob');

select folders.name, photos.file, tags.tag from folders, photos, tags where folders.id = photos.folder_id and photos.id = tags.photo_id;

# used to populate initial screen with list of folders
select name, folders.id, count(photos.id) from folders, photos where folders.id = photos.folder_id group by folders.id;

# start of retreiving photos for a specific folder
select photos.id, file, name as folder from photos join folders on folder_id = folders.id where folder_id = 173;

# get photos for a folder and associate tags with each photo
select file, array_agg (tag) tags from photos inner join tags on photos.id = tags.photo_id where folder_id = 1 group by file;

# with folder name - used in the application
## see source code for actual queries
select photos.id, file, array_agg (tag) tags, name as folder from photos left outer join tags on photos.id = tags.photo_id inner join folders on folders.id = photos.folder_id where folder_id = 176 group by photos.id, folders.name;

select distinct tag from tags order by tag asc;    # unqique tags across all photos

select distinct tag, count(tag) from tags group by tag;  # count of tags overall

# retrieve folders

# uses the view tagged_photos

# do not filter based on tag search
select distinct folder_id, folder, count(photo_id) from tagged_photos group by folder_id, folder order by folder asc;

# filter based on tag search
select distinct folder_id, folder, count(photo_id) from tagged_photos where 'bob' = any(tags) group by folder_id, folder order by folder asc;

## return photos for a folder - include all the tags for the photos

# uses the view tagged_photos

# do not filter based on tag search
select photo, photo_id, folder, folder_id, tags from tagged_photos where folder_id = 178 order by photo asc;

# filter based on tag search
select photo, photo_id, folder, folder_id, tags from tagged_photos where folder_id = 8 and 'kris' = any(tags) order by photo asc;
```

# Installing npm Node on the host

This method will replace the node.js installation in the container.  After this the container will no longer be needed.

## nvm to manage node versions

If nvm is not installed on the host install it:

https://github.com/nvm-sh/nvm

nvm install --lts

cd frontend
npm run build


# Frontend container (node.js for building frontend) (deprecated)

This container is not used for the running application.  It is used to build the assets and deploy them to the server container's host `build` directory.

# Creating the frontend container

The step to run the container will create the container if it does not exist.

See ~/vue_bootstrap_html_javascript.md for details on the container

# Working with the frontend container

## Run the container (create if it does not exist)
```
cd ~dev/photomanager
BASEDIR=$(pwd) UID=$(id -u) GID=$(id -g) docker compose -f ~dev/vue_dev_tools/docker-compose.yaml up -d
docker exec -t -i node-toolchain /bin/bash
cd frontend
npm run build   # deploys code to ../server/public
```

