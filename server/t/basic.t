use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

use Mojo::JSON qw(decode_json);
use Mojo::Collection;

use Data::Dumper;

my $t = Test::Mojo->new('PhotoManager');
$t->get_ok('/')->status_is(200)->content_like(qr/id="app"/i);

my $resp = $t->get_ok('/tags')->status_is(200)->content_like(qr/video/i)->tx->res->content->asset->slurp;

my $tags = Mojo::Collection->new( @{decode_json($resp)} );

ok($tags->size > 0, 'there should be at least one tag');

ok($tags->size == $tags->uniq()->size, 'tags should be unique');

ok(!$tags->first( sub { tr/a-z0-9\-//c } ), 'tags should be lower case letters and numbers only');
ok(!$tags->grep(qr/^[^a-z]+$/)->size, 'tags should have at least one leter');

done_testing();
