package PhotoManager::Controller::Main;

use Mojo::Base 'Mojolicious::Controller';

use File::Basename;

use Data::Dumper;

# This action will render a template
sub page {
  my $self = shift;

  $self->reply->static('index.html');
  #$self->render;
}

sub tags {
  my $self = shift;

  $self->render( json => $self->interface->get_tags() );
}

sub folders {
  my $self = shift;

  $self->render( json => $self->interface->get_folders( $self->param('searchTag') ) );
}

sub folder {
  my $self = shift;

  $self->render( json => $self->interface->get_folder( $self->param('folderId'),
                                                       $self->param('searchTag') ) )
}

sub photos {
  my $self = shift;

  $self->render( json => $self->interface->get_photos( $self->param('folderId'),
                                                       $self->param('searchTag') ) );
}

sub img {
  my $self = shift;

  my $photo_id = $self->param('photoId');  # folderId and version are sent for cache
                                           # busting only

  my $photo = $self->interface->photo( $self->param('photoId') );

  my $file = sprintf('../../..%s/%s/%s', $self->config->{basedir},
                                 $photo->{folder},
                                 $photo->{photo}
                     );

  #my (undef, undef, $suffix) = fileparse($file, qr/\.[^.]*$/);
  # if ($suffix =~ /mp4|mpg/) {
  #   $self->res->headers->content_type('video/mp4');
  # } else {
  #   $self->res->headers->content_type('image/jpeg');
  # }
  #$self->interface->debug("content type: " . $self->res->headers->content_type);

  $self->reply->static($file);
}

sub edit {
  my $self = shift;

  my $msg = $self->req->json;   # not sure why th "put" doesn't use params like "get"

  $self->render( json => $self->interface->edit( $msg->{photo_id}, $msg->{tags} ) );
}

sub delete {
  my $self = shift;

  $self->render( json => $self->interface->delete( $self->param('photoId') ) );
}

sub export_modify {
  my $self = shift;

  $self->render( json => $self->interface->export_modify( $self->param('action'),
                                                             $self->param('photoId'),
                                                             $self->param('exportFile') ) );
}

sub loaddb {
  my $self = shift;

  $self->interface->reload_db;

  $self->render( json => { msg => 'started' } );
}

sub upload {
  my $self = shift;

  my @files;
  for my $file (@{$self->req->uploads('file')}) {
    my $resp = $self->interface->upload($self->param('folderId'),
                                        $file
                                       );
    push @files, $resp;
  }
  $self->render(json => { files => [@files] });
}

sub create_folder {
  my $self = shift;

  $self->render( json => $self->interface->create_folder( $self->param('folder') ) );
}

# get a list of all export files (maybe add the listing of the file to the response)
sub export_names {
  my $self = shift;

  $self->render( json => $self->interface->export_names() );
}

# retrieve the export file and send it to the client
sub download_export {
  my $self = shift;

  my $zip = sprintf('../../..%s/%s', $self->config->{exportdir}, $self->param('exportFile'));

  $self->reply->static($zip);
}

1;
