package PhotoManager::Model::Interface;
use Mojo::Base -base;

use feature 'signatures';

use File::Find;
use File::Basename;

use Image::ExifTool;
use DateTime::Format::DateManip;
use Nice::Try;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

use Data::Dumper;

has postgres => sub { Carp::croak 'postgres is required' };
has log      => sub { Carp::croak 'log is required' };
has config   => sub { Carp::croak 'config is required' };

sub get_tags {
  my $self = shift;

  my $tags_hashes = $self->postgres->db->
    query('select distinct lower(tag) as tag from tags order by lower(tag) asc')->hashes->to_array;

  my @tags;
  foreach my $tag (@$tags_hashes) {
    push @tags, $tag->{tag};
  }

  return \@tags;
}

sub get_folders {
  my $self = shift;
  my $search = shift;

  $self->log->debug("get_folders search: -$search-");

  my $folders;
  if ($search eq '') {
    $folders = $self->postgres->db->
      query('select distinct folder_id, folder, count(photo_id) from tagged_photos group by folder_id, folder order by folder asc')->hashes->to_array;
  } else {
    $folders = $self->postgres->db->
      query('select distinct folder_id, folder, count(photo_id) from tagged_photos where ? ilike any(tags) group by folder_id, folder order by folder asc', $search)->hashes->to_array;
  }

  return $folders;
}

sub get_folder {
  my $self = shift;
  my $folder_id = shift;
  my $search = shift;

  my $folder;
  my $tags;
  try {
    $folder = $self->postgres->db->
      query('select distinct folder_id, folder, count(photo_id) from tagged_photos where folder_id = ? and (? = \'no_filter\' or ? ilike any(tags)) group by folder_id, folder', $folder_id, $search eq '' ? 'no_filter' : 'filter' , $search)->hash;

    if ($search eq '') {
      $tags = $self->postgres->db->
        query('select lower(tag) as tag, count(lower(tag)) from photos left outer join tags on photos.id = tags.photo_id inner join folders on folders.id = photos.folder_id where tag is not null and folders.id = ? group by folders.id, lower(tags.tag) order by folder, lower(tag) asc', $folder_id)->hashes->to_array;
    } else {
      $tags = []
    }
  }
  catch( $e ) {
    return $self->result( 'error',
        sprintf('Could not retrieve folder: %s', $e) );
  }

  foreach my $tag (@$tags) {
    $folder->{tags}{ $tag->{tag} } = $tag->{count}
  };

  return $folder;
}

sub get_photos {
  my $self = shift;
  my $folder_id = shift;
  my $search = shift;

  $self->log->debug("get_photos search: -$search-");

  # TODO: instead of removing nulls here, do not put them in tagged_photos
  my $photos;
  if ($search eq '') {
    $photos = $self->postgres->db->
      query('select photo, photo_id, image, folder, folder_id, array_remove(tags, null) as tags, created from tagged_photos where folder_id = ? order by photo asc', $folder_id)->hashes->to_array;
  } else {
    $photos = $self->postgres->db->
      query('select photo, photo_id, image, folder, folder_id, array_remove(tags, null) as tags, created from tagged_photos where folder_id = ? and ? ilike any(tags) order by photo asc', $folder_id, $search)->hashes->to_array;
  }

  return $photos;
}

sub reload_db {
  my $self = shift;

  Mojo::IOLoop->subprocess->run_p(sub {
    $self->clean_db;
    $self->init;
    return { msg => 'done with database reload' };
  })->then(sub {
    my $msg = shift;
  })->catch(sub {
    my $err = shift;
  });
}

sub init {
  my $self = shift;

  $self->{db} = $self->postgres->db;

  $self->log->info('Connected to: ' . $self->{db}->
    query('select version() as version')->hash->{version});

  # check if photos are loaded in the db
  my $folders = $self->{db}->query('select count(*) from folders')->hash->{count};

  if ($folders gt 0) {
    $self->log->info(sprintf("%d folders in db", $folders));
    return
  }

  $self->log->debug('Load process started');

  # load photos
  my $photos = $self->load_photos;
  $self->log->info("count of photos: " . $photos->size);

  # store folders and track folder IDs
  my %folders;
  my $num_folders = $photos->uniq( sub { $_->{folder} } )->each(sub {
    $self->log->debug("dir: " . $_->{folder});
    $folders{$_->{folder}} = $self->add_folder_to_db($_->{folder});
  })->size;
  $self->log->info("count of folders: " . $num_folders);

  eval {  # ignore error if transfer dir already added to db
    $self->add_folder_to_db($self->config->{transferdir});   # TODO:  not sure if this is used
  };

  # store photos and tags
  $photos->each(sub {
    $self->log->debug(sprintf('photo: %s%s',$_->{photo}, $_->{image} ? '' : ' (video)'));
    $self->add_photo_to_db($folders{$_->{folder}}, $_);
  });

  $self->log->info("Load process complete");
}

sub add_folder_to_db {
  my $self = shift;
  my $folder = shift;

  my $id = $self->{db}->
    query('insert into folders (id, folder) values (DEFAULT, ?) returning id', $folder)->
      hash->{id};

  return $id;
}

sub add_photo_to_db ($self, $folder_id, $photo) {

  my $name = $photo->{photo};
  my $image = $photo->{image};
  my $created = $photo->{exif}{CreateDate};
  my $tags = $photo->{tags};

  if ( (defined($created)) and
       (DateTime::Format::DateManip->parse_datetime( $created )) ) {
    $self->log->debug(sprintf('good date: %s', $created));
  } else {
    $self->log->debug(sprintf('invalid date or no date available: %s', $name));
    undef $created;
  }

  my $id = $self->{db}->
    query('insert into photos (id, folder_id, photo, image, created) values (DEFAULT, ?, ?, ?, ?) returning id', $folder_id, $name, $image, $created ? $created : undef)->hash->{id};

  foreach my $tag (@{$tags}) {
    $self->log->debug("tag: " . $tag);
    $self->add_tag_to_db($id, $tag);
  }

  return $id;
}

sub add_tag_to_db {
  my $self = shift;
  my $photo_id = shift;
  my $tag = shift;

  my $id = $self->{db}->
    query('insert into tags (id, photo_id, tag) values (DEFAULT, ?, ?) returning id', $photo_id, $tag)->
      hash->{id};

  return $id;
}

my $images;
sub load_photos {
  my $self = shift;

  $images = Mojo::Collection->new();

  find( sub { wanted_photos($self, $_) }, $self->config->{basedir});

  #$self->log->debug("images: " . Dumper($images->to_array));

  return $images;
}

sub split_tags {
  my $self = shift;
  my $file = shift;

  # remove extension
  $file =~ s/\.[^.]+$//;

  my @words = split(/_/, $file);
  @words = grep(!/MVI|DSC|dsc|IMG|img|vid|MG|PXL|mvi|crw|VID/, @words);

  @words = $self->filter_tags(\@words);

  return \@words;
}

sub is_image ($self, $file) {

  my $is_image = 1;
  my (undef, undef, $suffix) = fileparse($file, qr/\.[^.]*$/);
  if ($suffix =~ /mp4|mpg/) {
    $is_image = 0;
  }

  return $is_image;
}

my $exif_tool = Image::ExifTool->new;
sub extract_exif ($self, $path) {

  my %options = (DateFormat => '%Y-%m-%d %H:%M:%S');
  my @tagList = qw(exif:CreateDate);

  return $exif_tool->ImageInfo( $path, \@tagList, \%options);
}

sub wanted_photos {
  my $self = shift;
  my $file = shift;

  if ($file =~ /^\./) {  # dot files
    return;
  }

  if (-d $file) {  # directories
    return;
  }

  (my $parent = $File::Find::dir) =~ s/\/photos\///;

  if ($parent eq '/photos') {
    return;
  }

  my %media;

  $media{photo} = $file;
  $media{folder} = $parent;
  $media{tags} = $self->split_tags($file);
  $media{image} = $self->is_image($file);
  $media{exif} = $self->extract_exif(sprintf('%s/%s/%s', $self->config->{basedir}, $parent, $file));

  push @$images, \%media
}

sub photo {
  my $self = shift;
  my $photo_id = shift;

  $self->log->debug("getting photo details: " . $photo_id);

  my $db = $self->postgres->db;

  my $photo = $db->
    query('select folder, photo from folders, photos where folders.id = photos.folder_id and photos.id = ?', $photo_id)->hash;

  #$self->log->debug("from db: " . Dumper($photo));

  return $photo;
}

sub edit {
  my $self = shift;
  my $photo_id  = shift;
  my $tags = shift;

  $self->log->debug("photo_id to edit: " . $photo_id);

  # check tags early
  # see "wanted_photos" function for same tag check rules

  #return $self->validate_tags($tags)->get_photo($photo_id)->unique_name()->update_db();

  try {
    $self->validate_tags($tags);
  }
  catch( $e ) {
    return( $e );
  }

  my $db = $self->postgres->db;

  # retrieve current file name from the db

  my $photo;
  try {
    $photo = $db->
      query('select folder, photo, folder_id from folders, photos where folders.id = photos.folder_id and photos.id = ?', $photo_id)->hash;
    die 'no record in database' if not $photo
  }
  catch( $e ) {
    return $self->result( 'error',
        sprintf('Tag not changed (error getting photo metadata): "%s"', $e) );
  }

  # continue if file as cached in db exists

  my $current_name = sprintf('%s/%s/%s', $self->config->{basedir},
                                         $photo->{folder},
                                         $photo->{photo}
                            );

  $self->log->debug("checking: " . $current_name);

  if (! -e $current_name) {
    return $self->result( 'error',
        sprintf('Tag not changed ("%s" not found)', $current_name) )
  }

  # construct new file name based on front end and make sure it is unique

  my $new_photo;
  my $version;
  try {
    my $dir = sprintf('%s/%s/', $self->config->{basedir}, $photo->{folder});

    ($new_photo, $version) = $self->unique_name($photo->{photo}, $tags, $dir);

    $self->log->debug("renaming file to: " . $new_photo);

    $self->rename($current_name, "$dir$new_photo");
  }
  catch( $e ) {
    return $e;
  }

  # update database (update photo, replace tags)

  try {
    my $tx = $db->begin;

    $db->
      query('update photos set photo = ? where id = ?', $new_photo, $photo_id);
    $db->
      query('delete from tags where photo_id = ?', $photo_id);
    foreach my $tag (@{$tags}) {
      $db->
        query('insert into tags (id, photo_id, tag) values (default, ?, ?)', $photo_id, $tag);
    }

    $tx->commit;
  }
  catch( $e ) {
    return $self->result( 'error',
        sprintf('Database error for "%s" with id %d: (%s)',
          $new_photo, $photo_id, $e) );
  }

  return { photo => $new_photo,
           photo_id => $photo_id,
           folder => $photo->{folder},
           folder_id => $photo->{folder_id},
           tags => $tags,
           version => $version
         }
}

sub tag_ok {
  my $self = shift;
  my $tag = shift;

  return 0 if ($tag !~ /^[a-zA-Z0-9\-]+$/) || ($tag !~ /[a-zA-Z]+/);

  return 1
}

sub filter_tags {
  my $self = shift;
  my $tags = shift;

  my @ftags;
  foreach my $tag (@{$tags}) {
    if ($self->tag_ok($tag)) {
      push @ftags, $tag;
    }
  }

  return @ftags
}

sub validate_tags {
  my $self = shift;
  my $tags = shift;

  foreach my $tag (@{$tags}) {
    if (!$self->tag_ok($tag)) {
      die $self->result( 'error',
          sprintf('Invalid tag "%s" (tag not changed)', $tag) )
    }
  }
}

sub name ($self, $tags, $photo) {

  if (scalar(@{$tags}) == 0) {
    return 'img' if $self->is_image($photo);
    return 'vid';
  }

  return join('_', @{$tags});
}

sub count ($self, $tries) {

  return '' if $tries == 1;
  return sprintf('_%02d', $tries);
}

sub unique_name {
  my $self = shift;
  my $photo = shift;
  my $tags = shift;
  my $dir = shift;

  state $photo_counter = 0;  # used for cache busting
  $photo_counter++;

  my(undef, undef, $suffix) = fileparse($photo, qr/\.[^.]*$/);

  my $name;
  my $tries = 0;
  do {
    $tries++;
    $name = sprintf('%s%s%s', $self->name($tags, $photo), $self->count($tries), $suffix);
    $self->log->debug("testing file name: $name");
  } until (!-e sprintf('%s%s', $dir, $name));

  return $name, $photo_counter
}

sub rename {
  my $self = shift;
  my $from = shift;
  my $to = shift;

  my @args = ('/bin/mv', $from, $to);
  if (system(@args) != 0) {
    die $self->result( 'error',
        sprintf('Could not rename "%s" to "%s" (tag not changed)',
           $from, $to) );
  }
}

sub result {
  my $self = shift;
  my $type = shift;
  my $msg = shift;

  $self->log->info( sprintf('%s: %s', $type, $msg) );

  return { status => $type, msg => $msg }
}

sub delete {
  my $self = shift;
  my $photo_id = shift;

  $self->log->debug("deleting photo: " . $photo_id);

  my $db = $self->postgres->db;

  # delete photo from disk

  my $photo = $db->
    query('select folder, photo from folders, photos where folders.id = photos.folder_id and photos.id = ?', $photo_id)->hash;

  #$self->log->debug("from db: " . Dumper($photo));

  my $file = sprintf('%s/%s/%s', $self->config->{basedir},
                                 $photo->{folder},
                                 $photo->{photo}
                     );

  $self->log->debug("delete from disk: " . $file);
  unlink($file) or
    return $self->result( 'error',
        sprintf('Could not delete photo "%s" (%s)', $photo->{photo}, $!
      ) );

  # delete from the database

  eval {
    my $tx = $db->begin;

    $db->
      query('delete from tags where photo_id = ?', $photo_id);
    $db->
      query('delete from photos where id = ?', $photo_id);

    $tx->commit;
  };

  if ($@) {
    return $self->result( 'error',
        sprintf('Database error could not delete photo "%s" (%s)', $photo->{photo}, $@
      ) )
  }

  return $self->result( 'success',
      sprintf('Deleted photo %s', $photo->{photo} ) );
}

sub upload ($self, $folder_id, $file) {

  my $name = $file->filename;

  $self->log->debug(sprintf('uploading photo: %s to folder: %d', $name, $folder_id));

  # get folder name from $folder_id
  my $folder = $self->get_folder($folder_id, '');

  # parse tags from file name
  my %media;

  $media{folder} = $folder->{folder};
  $media{tags} = $self->split_tags($name);
  my $dir = sprintf('%s/%s/', $self->config->{basedir}, $folder->{folder});
  ($media{photo}, undef) = $self->unique_name($name, $media{tags}, $dir);
  $media{image} = $self->is_image($media{photo});

  $file->move_to( sprintf('%s/%s', $dir, $media{photo}) );

  $media{exif} = $self->extract_exif(sprintf('%s/%s', $dir, $media{photo}));

  $self->add_photo_to_db($folder_id, \%media);

  return \%media
}

# add photo to a zip archive
sub export_modify {
  my $self = shift;
  my $action = shift;
  my $photo_id = shift;
  my $export_file = shift;

  $self->log->debug($export_file);

  if (! $export_file) {
    return $self->result( 'error',
        sprintf('No export file defined') );
  }

  $self->log->debug(sprintf('%s photo %d', $action, $photo_id));

  my $zip_location = sprintf('%s/%s', $self->config->{exportdir}, $export_file);
  my $zip = Archive::Zip->new();
  if (-e $zip_location) {
    unless ( $zip->read( $zip_location ) == AZ_OK ) {
      return $self->result( 'error', sprintf('Could not read export file %s', $zip_location) );
    }
  }

  my $db = $self->postgres->db;

  my $photo;
  try {
    $photo = $db->
      query('select folder, photo from folders, photos where folders.id = photos.folder_id and photos.id = ?', $photo_id)->hash;
  }
  catch( $e ) {
    return $self->result( 'error', sprintf('Could not retreive photo %s', $e) );
  }

  if (! $photo) {
    return $self->result( 'error', sprintf('Could not retreive photo %s (id not found)', $photo_id) );
  }

  # construct file names for on disk and in zip file

  my $disk_name = sprintf('%s/%s/%s', $self->config->{basedir}, $photo->{folder}, $photo->{photo});
  my $zip_name = sprintf('%s/%s', $photo->{folder}, $photo->{photo});
  
  if ($action eq 'add') {
    # save the zip file

    # add a file from disk
    my $file_member = $zip->addFile( $disk_name, $zip_name );

    unless ( $zip->overwriteAs($zip_location) == AZ_OK ) {
      $self->log->error(sprintf('Could not write export file %s', $zip_location));
      return $self->result( 'error', sprintf('Could not write export file %s', $zip_location) );
    }

    return $self->result( 'success',
        sprintf('Export photo %s to %s', $photo_id, $export_file ) );
  } elsif ($action eq 'remove') {
    # remove the file from the zip file
    $zip->removeMember( $zip_name );

    unless ( $zip->overwriteAs($zip_location) == AZ_OK ) {
      $self->log->error(sprintf('Could not write export file %s', $zip_location));
      return $self->result( 'error', sprintf('Could not write export file %s', $zip_location) );
    }

    return $self->result( 'success',
        sprintf('Remove photo %s from %s', $photo_id, $export_file ) );
  } else {
    return $self->result( 'error', sprintf('Invalid action %s', $action) );
  }
}

sub export_names {
  my $self = shift;

  # get the zip files in the export directory and add id to each file
  my @names;
  my $id = 0;
  find ( sub {
      return unless -f;
      return unless /\.zip$/;
      push @names, { 'file' => $_, 'id' => $id };
      $id++;
  }, $self->config->{exportdir} );

  # for each zip file, get the names of the files in it
  foreach my $file (@names) {  
    my $zip_location = sprintf('%s/%s', $self->config->{exportdir}, $file->{file});
    my $zip = Archive::Zip->new();

    if (-e $zip_location) {
      unless ( $zip->read( $zip_location ) == AZ_OK ) {
        $self->log->error(sprintf('Could not read export file %s', $zip_location));
        next;
      }
    }

    $self->log->debug($zip_location);
    my @members = $zip->memberNames();
    my @contents;
    foreach my $member (@members) {   # add 'folder' and 'photo' to each member
      my @parts = $member =~ /^(.+)\/(.+)$/;
      my %content;
      $content{folder} = $parts[0];
      $content{photo} = $parts[1];
      push(@contents, \%content);
    }
    $file->{contents} = \@contents;
  }

  return { exports => \@names };
}

sub create_folder {
  my $self = shift;
  my $folder = shift;

  $self->log->debug(sprintf('creating folder: %s', $folder));

  # return error if already exists
  my $dir = sprintf('%s/%s/', $self->config->{basedir}, $folder);
  if (-e $dir) {
    return $self->result( 'error',
        sprintf('Folder already exists "%s"', $folder)
      )
  }

  # create folder on disk
  if (!mkdir($dir, 0775)) {
    return $self->result( 'error',
        sprintf('Could not create folder "%s" (%s)', $folder, $!)
      )
  }


  # create folder in db
  my $folder_id = $self->add_folder_to_db($folder);

  return {
           folder => $folder,
           folder_id => $folder_id
         }
}

sub clean_db {
  my $self = shift;

  $self->log->debug('beginning removal of all data');

  my $db = $self->postgres->db;

  try {
    my $folder = $db->
      query('truncate folders cascade');
  }
  catch( $e ) {
    $self->log->info("Data removal failed: $e");
    die $e;
  }

  $self->log->debug('data removal complete');


}

sub debug {
  my $self = shift;
  my $msg = shift;

  $self->log->debug(sprintf('%s', $msg));
}

1;
