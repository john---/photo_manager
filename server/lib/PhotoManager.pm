package PhotoManager;
use Mojo::Base 'Mojolicious', -signatures;

use Mojo::Pg;
use PhotoManager::Model::Interface;

# This method will run once at server start
sub startup ($self) {

  #$self->renderer->cache->max_keys(0);

  # Load configuration from config file
  my $config = $self->plugin('NotYAMLConfig');

  # Configure the application
  $self->secrets($config->{secrets});

  $self->helper( postgres => sub { state $pg = Mojo::Pg->new( $config->{pg} ) } );

  $self->helper(interface => sub { state $interface =
    PhotoManager::Model::Interface->new(
        postgres => $self->postgres,
        log      => $self->app->log,
        config   => $self->config,
    );
  });

  $ENV{MOJO_MAX_MESSAGE_SIZE} = 0;  # photo uploads are huge

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('Main#page');
  $r->get('/tags')->to('Main#tags');
  $r->get('/folders')->to('Main#folders');
  $r->get('/folder')->to('Main#folder');
  $r->get('/photos')->to('Main#photos');
  $r->get('/img')->to('Main#img');
  $r->put('/edit')->to('Main#edit');
  $r->get('/delete')->to('Main#delete');
  $r->get('/export_modify')->to('Main#export_modify');
  $r->get('/loaddb')->to('Main#loaddb');
  $r->post('/upload')->to('Main#upload');
  $r->get('/create_folder')->to('Main#create_folder');
  $r->get('/export_names')->to('Main#export_names');
  $r->get('/download_export')->to('Main#download_export');

  # thumb nails are TBD.  maybe use dynamic resizing
  #$r->get('/thumb/:dir/#file')->to('Main#thumb');

  $self->interface->init;
}

1;
